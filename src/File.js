'use strict';

const fs = require('fs'), os = require("os");

class File {

    constructor(){

        this.create = this.create;
        this.read = this.read;

    }

    /**
     * Synchronous creates new file, replacing the file if it already exists.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @param {string} content
     * @returns {File}
     */
    create(file, content = ''){
        return this.write(file, content, {encoding : 'utf8', flag : 'w'});
    }

    /**
     * Synchronous creates new file with Json content, replacing the file if it already exists.
     * @param {string} file
     * @param {object} json
     * @returns {File}
     */
    createJson(file, json = {}){
        return this.write(file, JSON.stringify(json), {encoding : 'utf8', flag : 'w'});
    }

    /**
     * Synchronous writes data to a file, replacing the file if it already exists.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @param {string} content
     * @param {string|Object} options
     * @link https://nodejs.org/dist/latest-v8.x/docs/api/fs.html#fs_fs_writefilesync_file_data_options
     * @returns {File}
     */
    write(file, content = '', options = {encoding : 'utf8', flag : 'a'}){
        fs.writeFileSync(file, content, options);
        return this
    }

    /**
     * Synchronous writes data to a file and adds empty line at end of file, replacing the file if it already exists.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @param {string} content
     * @param {string|Object} options
     * @link https://nodejs.org/dist/latest-v8.x/docs/api/fs.html#fs_fs_writefilesync_file_data_options
     * @returns {File}
     */
    writeln(file, content = '', options){
        return this.write(file, content+os.EOL, options)
    }

    /**
     * Synchronous writes empty line to a file, replacing the file if it already exists.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @param {int} count Number of lines.
     * @returns {File}
     */
    newLine(file, count = 1){
        return this.write(file, os.EOL.repeat(count))
    }

    /**
     * Synchronous removes a file.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @returns {File}
     */
    delete(file){
        fs.unlinkSync(file);
        return this
    }

    /**
     * Synchronous removes a file. Alias of delete method.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @returns {File}
     */
    remove(file){
        return this.delete(file)
    }

    /**
     * Synchronous copies a file.
     * @param {string|Buffer|URL|int} source Filename or file descriptor
     * @param {string|Buffer|URL|int} destination Filename or file descriptor
     * @returns {File}
     */
    copy(source, destination){
        // fs.createReadStream(source).pipe(fs.createWriteStream(resolve(destination)));
        this.create(destination, this.read(source));
        return this
    }

    /**
     * Synchronous renames a file.
     * @param {string|Buffer|URL|int} oldFile Filename or file descriptor
     * @param {string|Buffer|URL|int} newFile Filename or file descriptor
     * @returns {File}
     */
    rename(oldFile, newFile){
        fs.renameSync(oldFile, newFile);
        return this
    }

    /**
     * Checks if a file exists.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @returns {Boolean}
     */
    exists(file){
        try{
            return fs.statSync(file).isFile()
        }catch (e){
            return false
        }
    }

    /**
     * Synchronous reads a file content.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @param {string|Object} options
     * @returns {string}
     */
    read(file, options = {encoding : 'utf8'}){
         return fs.readFileSync(file, options)
    }

    /**
     * Synchronous reads a file content as Json.
     * @param {string|Buffer|URL|int} file Filename or file descriptor
     * @param {string|Object} options
     * @returns {string}
     */
    readJson(file, options){
        const content = this.read(file, options);
        return JSON.parse(content)
    }

}

module.exports = new File();