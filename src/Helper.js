class Helper {

    static getType(object) {
        if (object === null) {
            return null
        }
        let t = (typeof object);
        if (t === 'object') {
            object = String(object.constructor);
            if (/^(?:function|object) ([a-z0-9-]+)\(?/i.test(object)) {
                t = RegExp.$1;
                if (/^html[a-z]*element$/i.test(t)) {
                    t = 'element'
                }
            } else {
                t = undefined
            }
        }

        return t.substr(0, 1).toLowerCase() + t.substr(1)
    }

    static isString(object) {
        return this.getType(object) === "string";
    }

    static isNumber(object) {
        return this.getType(object) === "number";
    }

    static isArray(object) {
        return this.getType(object) === "array";
    }

    static isObject(object) {
        return this.getType(object) === "object";
    }

    static isBoolean(object) {
        return this.getType(object) === "boolean";
    }

    static isElement(object) {
        return getType(object) === "element";
    }

    static isFunction(object) {
        return this.getType(object) === "function";
    }

    static isCallback(object) {
        return this.isFunction(object);
    }

    /**
     * Returns true for empty array, string, object, false, undefined, 0, null, NaN
     *
     * @param object
     * @return {boolean}
     */
    static isEmpty(object) {
        if (!object) {
            return true
        }
        for (let k in object) {
            if (object.hasOwnProperty(k)) {
                return false
            }
        }

        if (object === true || this.isNumber(object)) {
            return false
        }

        return true;
    }

    static isNull(object) {
        return object === null;
    }

    static isFalse(object) {
        return object === false;
    }

    static isTrue(object) {
        return object === true;
    }

    static isRegExp(object) {
        return this.getType(object) === "regexp";
    }

    static convertToArray(o) {

        if (this.isObject(o)) {
            let res = [];
            for (let k in o) {
                if (o.hasOwnProperty(k)) {
                    res.push(o[k]);
                }
            }
            return res;
        }

        return [].slice.call(o);

    }

    static isWindows(){
        return process.platform === 'win32';
    }

    static isLinux(){
        return process.platform === 'linux';
    }

    static isMac(){
        return process.platform === 'darwin';
    }

    static isInux(){
        return this.isLinux() || this.isMac();
    }

    static getUserHome(){
        return process.env[this.isWindows() ? 'USERPROFILE' : 'HOME'];
    }

    static lowerFirst(text){
        return (text.slice(0, 1)).toLowerCase() + text.slice(1);
    }

    static upperFirst(text){
        return (text.slice(0, 1)).toUpperCase() + text.slice(1);
    }

}

module.exports = Helper;