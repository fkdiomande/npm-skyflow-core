'use strict';

const fs = require('fs'), path = require('path'), Helper = require('./Helper');

class Directory {

    /**
     * Synchronous creates new directory.
     * @param {string} directory
     * @returns {Directory}
     */
    create(directory){

        const dirs = directory.split(/[\\\/]/);

        directory = '';

        dirs.every((dir)=>{
            directory += dir + path.sep;
            if(!this.exists(directory)){
                fs.mkdirSync(directory);
            }
            return true
        });

        return this
    }

    /**
     * Synchronous removes new directory.
     * @param {string} directory
     * @returns {Directory}
     */
    delete(directory){

        if(!this.exists(directory)){
            return this
        }

        const files = fs.readdirSync(directory);

        files.every((dir)=>{
            dir = directory + path.sep + dir;

            if(this.exists(dir)){
                this.delete(dir)
            }else {
                fs.unlinkSync(dir)
            }

            return true

        });

        fs.rmdirSync(directory)

    }

    /**
     * Synchronous removes new directory. Alias of delete method.
     * @param {string} directory
     * @returns {Directory}
     */
    remove(directory){
        return this.delete(directory)
    }

    /**
     * Synchronous copies a directory.
     * @param {string} source
     * @param {string} destination
     * @returns {Directory}
     */
    copy(source, destination){

        this.create(destination);

        const files = fs.readdirSync(source);

        files.every((dir)=>{
            let dest = destination + path.sep + dir;
            dir = source + path.sep + dir;

            if(this.exists(dir)){
                this.copy(dir, dest)
            }else {
                fs.createReadStream(dir).pipe(fs.createWriteStream(dest))
            }

            return true

        });

        return this

    }

    /**
     * Synchronous renames a directory.
     * @param {string} oldDirectory
     * @param {string} newDirectory
     * @returns {Directory}
     */
    rename(oldDirectory, newDirectory){
        fs.renameSync(oldDirectory, newDirectory);
        return this
    }

    /**
     * Checks if directory exists.
     * @param {string} directory
     * @returns {Boolean}
     */
    exists(directory){
        try{
            return fs.statSync(directory).isDirectory()
        }catch (e){
            return false
        }

    }

    /**
     * Synchronous reads a directory.
     * @param {string} directory
     * @param {object} options
     * @returns {array}
     */
    read(directory, options = {directory: true, file: true, filter: /.*/}){

        if(!Helper.isObject(options)){options = {}}

        if(!Helper.isBoolean(options.directory)){options.directory = true}

        if(!Helper.isBoolean(options.file)){options.file = true}

        if(!Helper.isRegExp(options.filter)){options.filter = /.*/}

        let result = [];

        const files = fs.readdirSync(directory);

        files.every((dir)=>{

            if(!options.filter.test(dir)){
                return true
            }

            let d = directory + path.sep + dir;

            if(this.exists(d) && options.directory){
                result.push(dir)
            }

            if(!this.exists(d) && options.file){
                result.push(dir)
            }

            return true

        });

        return result
    }

}

module.exports = new Directory();