class Validator {

    /**
     * Validator constructor
     * @param {RegExp} validator
     * @param {String} errorMessage
     */
    constructor(validator, errorMessage = ''){
        this.validator = validator;
        this.errorMessage = errorMessage;
    }

    /**
     * Gets validator
     * @returns {RegExp}
     */
    getValidator(){
        return this.validator
    }

    /**
     * Gets error message
     * @returns {String}
     */
    getErrorMessage(){
        return this.errorMessage
    }

    /**
     * Checks if text is valid
     * @param {String} text
     * @returns {boolean}
     */
    isValid(text){
        return this.validator.test(text)
    }

}


module.exports = Validator;