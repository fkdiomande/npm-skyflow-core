'use strict';

const Handle = {
    File: require('./src/File'),
    Validator: require('./src/Validator'),
    Directory: require('./src/Directory'),
    Helper: require('./src/Helper'),
};

global.Skyflow = Handle;