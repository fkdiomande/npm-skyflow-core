<div align="center">
    <a href="https://skyflow.io/">
        <img width="300" src="extra/images/skyflow.png">
    </a>
</div>

<h1 align="center">Core library of skyflow, used in Skyflow CLI</h1>

This repository contains the **core functions** and tools used in Skyflow CLI. It was split to organize the developments and take advantage of NPM dependencies management.

You can check [Skyflow CLI repository](https://github.com/skyflow-io/npm-skyflow-cli) for more details about Skyflow and our related [documentation](https://skyflow.io/doc).

You are free to use **skyflow-core** in your own projects. It provides you tools and helpers for your Node.js developments.

## Prerequisites

**Skyflow-core** is developed in Nodejs. 

You need [Nodejs](https://nodejs.org) version >=6.0.0 or [Yarn](https://yarnpkg.com).

## Require skyflow-core in your project

_**With npm**_

```
npm install skyflow-core
```

_**With yarn**_

```
yarn add skyflow-core
```

## Documentation

The documentation for Skyflow core has not been implemented yet. We are focusing on [Skyflow CLI](https://skyflow.io/doc#doc-for-default-module-what-the-point) tools.

## Contributing

We are working on the contributing model for Skyflow, it's coming soon 😀
